const saveButton = document.querySelector('.saveButton');
const addButton = document.querySelector('.add');
const editForm = document.querySelector('.form');

const users = localStorage.getItem('users') === null
    || localStorage.getItem('users') === undefined
    || JSON.parse(localStorage.getItem('users')).length === 0
  ? usersData
  : JSON.parse(localStorage.getItem('users'));

function callForm(flag, data) {
  editForm.style.display = 'flex';

  const findBiggestId = () => {
    const ids = users.length !== 0
      ? users.map((object) => object.id)
      : [0];
    const maxId = Math.max(...ids);
    return maxId + 1;
  };

  const nextId = findBiggestId();

  if (flag === 'edit') {
    document.getElementById('name').value = data.name;
    document.getElementById('password').value = data.password;
    document.getElementById('age').value = data.age;
    document.getElementById('email').value = data.email;
    document.getElementById('phone').value = data.phone;
    document.getElementById('bankCard').value = data.bankCard;
  }

  const formValue = document.querySelector('.form').elements;

  saveButton.addEventListener('click', (event) => {
    event.preventDefault();

    const editedUserData = {
      id: flag === 'edit' ? data.id : nextId,
      name: formValue.name.value,
      password: formValue.password.value,
      age: formValue.age.value,
      email: formValue.email.value,
      phone: formValue.phone.value,
      bankCard: formValue.bankCard.value,
    };

    const checkedUserData = validateForm(editedUserData);

    if (checkedUserData === 'valid') {
      if (flag === 'edit') {
        const editedUserId = users.findIndex((el) => el.id === data.id);
        users[editedUserId] = editedUserData;
        const usersString = JSON.stringify(users);
        localStorage.setItem('users', usersString);
      } else {
        users.push(editedUserData);
        const usersString = JSON.stringify(users);
        localStorage.setItem('users', usersString);
      }
      editForm.style.display = 'none';
      location.reload();
    } else {
      buttonError.innerHTML = 'Check fields';
    }
  });
}

function viewUserData(data) {
  const dataContainer = document.querySelector('.data-container');

  dataContainer.innerHTML = `<div>
<div>name: ${data.name}</div>
<div>password: ${data.password}</div>
<div>age: ${data.age}</div>
<div>email: ${data.email}</div>
<div>phone: ${data.phone}</div>
<div>bankCard: ${data.bankCard}</div>
</div>`;

  dataContainer.appendChild(dataContainer);
}

function renderUsers() {
  const usersContainer = document.querySelector('.container');

  for (let i = 0; i < users.length; i++) {
    const user = document.createElement('div');
    user.textContent = users[i].name;
    usersContainer.appendChild(user);

    const email = document.createElement('div');
    email.textContent = users[i].email;
    usersContainer.appendChild(email);

    const editButton = document.createElement('button');
    const viewButton = document.createElement('button');
    const deleteButton = document.createElement('button');
    editButton.textContent = 'edit';
    viewButton.textContent = 'view';
    deleteButton.textContent = 'remove';
    usersContainer.appendChild(editButton);
    usersContainer.appendChild(viewButton);
    usersContainer.appendChild(deleteButton);

    viewButton.addEventListener('click', () => {
      viewUserData(users[i]);
    });
    editButton.addEventListener('click', () => {
      callForm('edit', users[i]);
    });

    deleteButton.addEventListener('click', () => {
      document.querySelector('.modal').style.display = 'flex';
      document.querySelector('.modalButton').addEventListener('click', () => {
        usersContainer.removeChild(user);
        usersContainer.removeChild(editButton);
        usersContainer.removeChild(viewButton);
        usersContainer.removeChild(deleteButton);

        const updatedArray = users.filter((el) => el.id !== users[i].id);
        const updatedArrayString = JSON.stringify(updatedArray);
        localStorage.setItem('users', updatedArrayString);
        location.reload();
      });
      document.querySelector('.closeButtonCancel').addEventListener('click', () => {
        document.querySelector('.modal').style.display = 'none';
      });
    });
  }
}

addButton.addEventListener('click', () => {
  callForm('add');
});
renderUsers();
