const usersData = [

  {
    id: 1,
    name: 'Paul',
    password: 'yellow',
    age: 29,
    email: 'paul@gmail.com',
    phone: 1234567890,
    bankCard: 1234567890123456,
  },
  {
    id: 2,
    name: 'John',
    password: 'orange',
    age: 27,
    email: 'john@gmail.com',
    phone: 1324567890,
    bankCard: 2365678112345687,
  },
  {
    id: 3,
    name: 'Jack',
    password: 'violet',
    age: 32,
    email: 'jack@gmail.com',
    phone: 5324123660,
    bankCard: 3456789123456789,
  },
];
